
  function startDefaultSwiper(){
    mainSwiper= new Swiper(".swiper-container", {
        speed: 400,
        spaceBetween: 100,
        loop: true,
        slidesPerView: 1,
        pagination: ".swiper-container .swiper-pagination",
        paginationClickable: true,
        autoplay: 3000
    });
  }

  function startMobileSwiper(){
    mobileSwiper = new Swiper(".swiper-container-mobile", {
        speed: 400,
        spaceBetween: 100,
        slidesPerView: 1,
        pagination: ".swiper-container-mobile .swiper-pagination",
        paginationClickable: true,
        autoplay: 3000
    });
  }

function startSwiper () {
    jQuery('body').addClass('opt9');
    var html = `
        <div class="optContainer container">
            <div class="swiper-container-mobile"> 
                <div class="swiper-wrapper"> 
                    <div class="swiper-slide"><a href="https://www.tafecourses.com.au/about/"><img src="https://image.ibb.co/nDGD9v/slider1mob.png"></a></div>
                    <div class="swiper-slide"><a href="https://www.tafecourses.com.au/courses/"><img src="https://image.ibb.co/hS5vNF/slider2mob.png"></a></div>
                    <div class="swiper-slide"><a href="https://www.tafecourses.com.au/kw/"><img src="https://image.ibb.co/i0B22F/slide3mob.png"></a></div>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
        <div class="optContainer container">
            <div class="swiper-container"> 
                <div class="swiper-wrapper"> 
                    <div class="swiper-slide"><a href="https://www.tafecourses.com.au/about/"><img src="https://image.ibb.co/cmcyba/slide1.png"></a></div>
                    <div class="swiper-slide"><a href="https://www.tafecourses.com.au/courses/"><img src="https://image.ibb.co/f8rD9v/slide2.png"></a></div>
                    <div class="swiper-slide"><a href="https://www.tafecourses.com.au/kw/"><img src="https://image.ibb.co/g82Lpv/slide3.png"></a></div>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    `
    
    jQuery('.homepage-search-course').before(html);
    jQuery('.homepage-search-course').remove();
    startMobileSwiper();
    startDefaultSwiper();

}

$("head link[rel='stylesheet']").last().after('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.0/css/swiper.min.css">');
function getScript(src, callback) {
    var s = document.createElement('script');
    s.src = src;
    s.async = true;
    s.onreadystatechange = s.onload = function() {
      if (!callback.done && (!s.readyState || /loaded|complete/.test(s.readyState))) {
        callback.done = true;
        callback();
      }
    };
    document.querySelector('head').appendChild(s);
  }
  
  getScript("https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.0/js/swiper.min.js",startSwiper);

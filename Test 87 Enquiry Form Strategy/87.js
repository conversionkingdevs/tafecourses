function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector) }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector) }, 50);
    }    
}

function start(){
    var variation = 1;
    switch(variation){
        case 0: 
            runvar1();
            break;
        case 1: 
            runvar2();
            break;
        default: 
            console.log("invalid variation");
            break;
    }
}

/**************************************
    POPUP VARIATION 1
**************************************/
function runvar1(){
    $(".OptimizelyTest .enquiry-form .gform_fields > li:lt(5)").wrapAll("<div class='first-inputs' />");
    $(".OptimizelyTest .enquiry-form .gform_fields > li").wrapAll("<div class='last-inputs' />");
    $(".OptimizelyTest .gform_wrapper .course-enquiry-form .gform_footer ").prepend("<a class='gform_button next_button button'>Next <i class='fa fa-caret-right'></i></a>");
    
    $(".OptimizelyTest .next_button").click(function(e){
        e.preventDefault();
        errors = false;
        $(".OptimizelyTest .first-inputs li").removeClass('gfield_error');
        $(".OptimizelyTest .first-inputs li .validation_message").remove();
        $(".OptimizelyTest .first-inputs li").each(function(){
            $li = $(this);
            if(!$li.find('input').val()){
                errors = true;
                $li.addClass('gfield_error');
                $li.append('<div class="gfield_description validation_message">This field is required.</div>');
            } 
        });
        
        if(errors == false){
            $(".OptimizelyTest .first-inputs").hide();
            $(".OptimizelyTest .last-inputs").show();
            $(".OptimizelyTest .gform_wrapper .course-enquiry-form .gform_button").show();
            $(".OptimizelyTest .gform_wrapper .course-enquiry-form .gform_button.next_button").hide();
            $(".OptimizelyTest .enquiry-form .terms").show();
        };    
    });
    
    
    $(".OptimizelyTest #course-enquiry-toogle, .OptimizelyTest .course-guide-button").click(function(){
        $(".OptimizelyTest .first-inputs").show();
        $(".OptimizelyTest .last-inputs").hide();
        $(".OptimizelyTest .gform_wrapper .course-enquiry-form .gform_button").hide();
        $(".OptimizelyTest .gform_wrapper .course-enquiry-form .gform_button.next_button").show();
        $(".OptimizelyTest .enquiry-form .terms").hide();
    });
    
}



/**************************************
    POPUP VARIATION 2 TESTIMONIALS
**************************************/
function runvar2(){
    $(".OptimizelyTest .enquiry-content").append("<div class='enquiry-testimonials' />");
    $(".OptimizelyTest .enquiry-content .enquiry-testimonials").append("<div class='enquiry-testimonials-header' />");
    $(".OptimizelyTest .enquiry-content .enquiry-testimonials").append("<div class='enquiry-testimonials-list' />");
    
    $(".OptimizelyTest .enquiry-content .enquiry-testimonials .enquiry-testimonials-header").append("<h2>Receive a consultation from one of our course experts</h2>");
    $(".OptimizelyTest .enquiry-content .enquiry-testimonials .enquiry-testimonials-header").append("<p>See our past student’s testimonials below</p>");
    
    $(".OptimizelyTest .enquiry-content .enquiry-testimonials .enquiry-testimonials-list").append("<div class='testimonial-item'><img src='//useruploads.visualwebsiteoptimizer.com/useruploads/302000/images/96bde81f3d46244124c9cd2a31302494_bela.png' /><blockquote>I found the website easy to use and the contact I received after my enquiry was good.  Not too pushy but there if I needed them. <cite>- Dimity Gill</cite></blockquote></div>");
    
    $(".OptimizelyTest .enquiry-content .enquiry-testimonials .enquiry-testimonials-list").append("<div class='testimonial-item'><img src='//useruploads.visualwebsiteoptimizer.com/useruploads/302000/images/4ed4fe39b12b7085baf9899c04bfeebf_celeste.png' /><blockquote>All details and info was there and my query was answered quickly. <cite>-  Marlon Marasigan</cite></blockquote></div>");
    
    $(".OptimizelyTest .enquiry-content .enquiry-testimonials .enquiry-testimonials-list").append("<div class='testimonial-item'><img src='//useruploads.visualwebsiteoptimizer.com/useruploads/302000/images/3938f1f65423303e75c20589ec8f66ae_john.png' /><blockquote>Gave me everything I needed and someone called me, pretty much straight away. <cite>- Soula Tsekouras</cite></blockquote></div>");
    
}

defer(function(){
    $ = jQuery.noConflict();
    $('body').addClass('OptimizelyTest');
    start();
}, ".product");

/* CUSTOM CODE */
function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector) }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector) }, 50);
    }    
}
function start(){
    var variation = 0;
    switch(variation){
        case 0: 
            runvar1();
            break;
        case 1: 
            runvar2();
            break;
        default: 
            console.log("invalid variation");
            break;
    }
}
/**************************************
    POPUP VARIATION 1
**************************************/
function runvar1(){
    $(".OptimizelyTest .enquiry-form .gform_fields > li:lt(5)").wrapAll("<div class='first-inputs' ></div>");
    $(".OptimizelyTest .enquiry-form .gform_fields > li").wrapAll("<div class='last-inputs' ></div>");
    $(".OptimizelyTest .gform_wrapper .course-enquiry-form .gform_footer ").prepend("<a class='gform_button next_button button'>Next <i class='fa fa-caret-right'></i></a>");
    
    $(".OptimizelyTest .first-inputs").show();
    $(".OptimizelyTest .last-inputs").hide();
    $(".OptimizelyTest .gform_wrapper .course-enquiry-form .gform_button").hide();
    $(".OptimizelyTest .gform_wrapper .course-enquiry-form .gform_button.next_button").css("display", "block");
    $(".OptimizelyTest .enquiry-form .terms").hide();
    $(".OptimizelyTest #optOne").show();
    $(".OptimizelyTest #optTwo").hide();

    $(".OptimizelyTest .next_button").click(function(e){
        e.preventDefault();
        errors = false;
        $(".OptimizelyTest .first-inputs li").removeClass('gfield_error');
        $(".OptimizelyTest .first-inputs li .validation_message").remove();
        $(".OptimizelyTest .first-inputs li").each(function(){
        $li = $(this);
        if(!$li.find('input').val()){
            errors = true;
            $li.addClass('gfield_error');
            $li.append('<div class="gfield_description validation_message">This field is required.</div>');
        } 
    });
        
    if(errors == false){
        $(".OptimizelyTest .first-inputs").hide();
        $(".OptimizelyTest .last-inputs").show();
        $(".OptimizelyTest .gform_wrapper .course-enquiry-form .gform_button").show();
        $(".OptimizelyTest .gform_wrapper .course-enquiry-form .gform_button.next_button").hide();
        $(".OptimizelyTest .enquiry-form .terms").show();
        $(".OptimizelyTest #optOne").hide();
        $(".OptimizelyTest #optTwo").show();
    };    
    });
    
    
    $(".OptimizelyTest #course-enquiry-toogle, .OptimizelyTest .course-guide-button").click(function(){
        $(".OptimizelyTest .first-inputs").show();
        $(".OptimizelyTest .last-inputs").hide();
        $(".OptimizelyTest .gform_wrapper .course-enquiry-form .gform_button").hide();
        $(".OptimizelyTest .gform_wrapper .course-enquiry-form .gform_button.next_button").show();
        $(".OptimizelyTest .enquiry-form .terms").hide();
      	$(".OptimizelyTest #optOne").show();
        $(".OptimizelyTest #optTwo").hide();
    });
  	$('.OptimizelyTest #popup-container .enquiry-form').append('<div class="optNumbers"><div id="optOne">1/2</div><div style="display:none;" id="optTwo">2/2</div></div>');
}
/**************************************
    POPUP VARIATION 2 TESTIMONIALS
**************************************/
function runvar2(){
    $(".OptimizelyTest .enquiry-content").append("<div class='enquiry-testimonials' ></div>");
    $(".OptimizelyTest .enquiry-content .enquiry-testimonials").append("<div class='enquiry-testimonials-header' ></div>");
    $(".OptimizelyTest .enquiry-content .enquiry-testimonials").append("<div class='enquiry-testimonials-list' ></div>");
    
    $(".OptimizelyTest .enquiry-content .enquiry-testimonials .enquiry-testimonials-header").append("<h2>Receive a consultation from one of our course experts</h2>");
    $(".OptimizelyTest .enquiry-content .enquiry-testimonials .enquiry-testimonials-header").append("<p>See our past student’s testimonials below</p>");
    
    $(".OptimizelyTest .enquiry-content .enquiry-testimonials .enquiry-testimonials-list").append("<div class='testimonial-item'><img src='https://i.imgur.com/J0jdTug.png' /><blockquote>A very interesting and valuable experience.  I learnt a lot and it gave me a good grounding in the basics. It really made me think, and I liked how it enabled me to interact with people through the practical work. <cite>- Sandra Burn</cite></blockquote></div>");
    
    $(".OptimizelyTest .enquiry-content .enquiry-testimonials .enquiry-testimonials-list").append("<div class='testimonial-item'><img src='https://i.imgur.com/hkOmqIr.png' /><blockquote>A very interesting and valuable experience.  I learnt a lot and it gave me a good grounding in the basics. It really made me think, and I liked how it enabled me to interact with people through the practical work. <cite>- Olivia Jane</cite></blockquote></div>");
    
    $(".OptimizelyTest .enquiry-content .enquiry-testimonials .enquiry-testimonials-list").append("<div class='testimonial-item'><img src='https://i.imgur.com/tN0mKxd.png' /><blockquote>A very interesting and valuable experience.  I learnt a lot and it gave me a good grounding in the basics. It really made me think, and I liked how it enabled me to interact with people through the practical work. <cite>- Dora Evans</cite></blockquote></div>");
}
defer(function(){
    $ = jQuery.noConflict();
    $('body').addClass('OptimizelyTest');
    start();
}, ".product");

desktopImages = {
    "oneimage": "//useruploads.visualwebsiteoptimizer.com/useruploads/302000/images/a96dc635bbd6ad1f6e3f277d9c593db2_marlon.png",
    "twoimage": "//useruploads.visualwebsiteoptimizer.com/useruploads/302000/images/3e20712929a75207c09c8afde4031eea_dimity.png",
    "threeimage": "//useruploads.visualwebsiteoptimizer.com/useruploads/302000/images/cfe8bdcb9566a632d532326b21cc21d4_soula.png",
    "fourimage": "//useruploads.visualwebsiteoptimizer.com/useruploads/302000/images/255e09ec6d17d8e5ff05c4c35aff88ac_kylie.png"
};
var testimonials_data = {
    'testimonials' : {
        '1' : {
            'author':       'Dimity G',
            //'subtitle':     '&nbsp;&nbsp;|&nbsp;&nbsp;2016 Graduate',
            //'position':     'Certificate IV in Accounting',
            'quote':        'I found the website easy to use and the contact I received after my enquiry was good.  Not too pushy but there if I needed them.',
            'avatar':       desktopImages['oneimage']
        },
        '2' : {
            'author':       'Marlon M',
            //'subtitle':     '&nbsp;&nbsp;|&nbsp;&nbsp;2016 Graduate',
            //'position':     'Certificate IV in Accounting',
            'quote':        'All details and info was there and my query was answered quickly.',
            'avatar':       desktopImages['twoimage']
        },
        '3' : {
            'author':       'Soula T',
            //'subtitle':     '&nbsp;&nbsp;|&nbsp;&nbsp;2016 Graduate',
            //'position':     'Certificate IV in Accounting',
            'quote':        'Gave me everything I needed and someone called me, pretty much straight away.',
            'avatar':       desktopImages['threeimage']
        },
        '4' : {
            'author':       'Kylie B',
            //'subtitle':     '&nbsp;&nbsp;|&nbsp;&nbsp;2016 Graduate',
            //'position':     'Certificate IV in Accounting',
            'quote':        'Using this website to find a course I am considering as my career was made very easy to do, they made it a easy experience to make sure I was making the right steps into my future. I am pleased to have come across this website and would highly recommended it to anyone who is looking to find the right course.',
            'avatar':       desktopImages['fourimage']
        }   
    }
};

function Testimonials(data){
    
        /* Styles */
        $("body").addClass("OptimizelyTest");
        $('body').addClass('opt91');
        $('.opt91 .course-content .left-content .job-outcomes:eq(0)').prepend('<div class="testimonials-wrap opt91-testbox opt91-mobile" id="menu-testimonials"><div class="testimonials"></div></div>');
        
        $(".opt91 .testimonials").append('<div class="testimonials-navigation" ></div>');
        $(".opt91 .testimonials").append('<div class="testimonials-container"></div>');

        var i=1; while(data['testimonials'][i]){
            $(".opt91 .testimonials .testimonials-navigation").append('<div class="link"><a href="#testimonial-'+i+'"><img src="'+data['testimonials'][i]['avatar']+'" /></a></div>');
            $(".opt91 .testimonials .testimonials-container").append('<div class="tab" id="testimonial-'+i+'" ></div>');
            $(".opt91 .testimonials .testimonials-container #testimonial-"+i+"").append('<div class="quote"><div class="inner"></div></div>');
            $(".opt91 .testimonials .testimonials-container #testimonial-"+i+" .image").append('<img src="'+data['testimonials'][i]['image']+'" /><img class="mobile-image" src="'+data['testimonials'][i]['mobile_image']+'" />');
            $(".opt91 .testimonials .testimonials-container #testimonial-"+i+" .quote .inner").append('<h2>'+data['testimonials'][i]['author']+'</h2>');
            //$(".opt91 .testimonials .testimonials-container #testimonial-"+i+" .quote .inner").append('<h2>'+data['testimonials'][i]['author']+'<span>'+data['testimonials'][i]['subtitle']+'</span></h2>');
            //$(".opt91 .testimonials .testimonials-container #testimonial-"+i+" .quote .inner").append('<h3>'+data['testimonials'][i]['position']+'</h3>');
            $(".opt91 .testimonials .testimonials-container #testimonial-"+i+" .quote .inner").append('<blockquote>'+data['testimonials'][i]['quote']+'</blockquote>');
            $(".opt91 .testimonials .testimonials-container #testimonial-"+i+"").append('<div class="clear" ></div>');
            i++;
        }
        $(".opt91 .testimonials").append('<div class="clear" ></div>');
        
        
        $(".testimonials-container").css('min-height',$(".testimonials-navigation").outerHeight(true));
        
        $(".testimonials-navigation .link").first().addClass('active');
        $(".testimonials-container #testimonial-1").addClass('active');
        setTimeout(function(){
            $(".testimonials-container #testimonial-1").animate({opacity:1},300,function(){
                $(".testimonials-container #testimonial-1 .quote").css('margin-top',($(".testimonials-container #testimonial-1").height() - $('#testimonial-1 .quote').height()) / 2);    
            });
        },500);
        setTimeout(function(){
            $(".testimonials-container #testimonial-1").animate({opacity:1},300,function(){
                $(".testimonials-container #testimonial-1 .quote").css('margin-top',($(".testimonials-container #testimonial-1").height() - $('#testimonial-1 .quote').height()) / 2);    
            });
        },1000);
        setTimeout(function(){
            $(".testimonials-container #testimonial-1").animate({opacity:1},300,function(){
                $(".testimonials-container #testimonial-1 .quote").css('margin-top',($(".testimonials-container #testimonial-1").height() - $('#testimonial-1 .quote').height()) / 2);    
            });
        },1500);
        
        
        $(".testimonials-navigation a").click(function(e){
            e.preventDefault();
            $this = $(this);
            $href = $this.attr('href');
            $(".testimonials-navigation .link").removeClass('active');
            $this.parent().addClass('active');
            $(".testimonials-container .tab").animate({opacity:0},300,function(){
               $(".testimonials-container .tab").removeClass('active').hide();
               $($href).addClass('active');
               $($href).find('.quote').css('margin-top',($($href).height() - $($href).find('.quote').height()) / 2); 
               $($href).animate({opacity:1},300);
              
            });
            //testimonial interaction
        });
    }
;

 Testimonials(testimonials_data);
if (window.innerWidth <= 768) {
        if ($('.opt91-testbox').hasClass('opt91-desktop')) {
            $('.opt91-testbox').addClass('opt91-mobile');
            $('.opt91-testbox').removeClass('opt91-desktop');
            $('.opt91-testbox').insertBefore('.opt91 .course-content .left-content .job-outcomes');
        }
     } else if (window.innerWidth > 768) {
        if ($('.opt91-testbox').hasClass('opt91-mobile')) {
            $('.opt91-testbox').addClass('opt91-desktop');
            $('.opt91-testbox').removeClass('opt91-mobile');
            $('.opt91-testbox').appendTo('.opt91 .entry-content aside:eq(0)');
        }
    }
window.onresize = function(event) {
        if (window.innerWidth <= 768) {
            if ($('.opt91-testbox').hasClass('opt91-desktop')) {
                $('.opt91-testbox').addClass('opt91-mobile');
                $('.opt91-testbox').removeClass('opt91-desktop');
                $('.opt91-testbox').insertBefore('.opt91 .course-content .left-content .job-outcomes');
            }
         } else if (window.innerWidth > 768) {
            if ($('.opt91-testbox').hasClass('opt91-mobile')) {
                $('.opt91-testbox').addClass('opt91-desktop');
                $('.opt91-testbox').removeClass('opt91-mobile');
                $('.opt91-testbox').appendTo('.opt91 .entry-content aside:eq(0)');
            }
        }
    };

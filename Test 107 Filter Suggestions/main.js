
function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
           
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

function noFilter(){
    var count =  window.location.pathname.split("/").length;
    if(count == 4) {
        return true;
    }
    return false;
}

function saveLevel(){
   
    var data = [];
    jQuery("div[data-name='level'] a").each(function(){
        var temp = {
            "name": jQuery(this).find("label")[0].childNodes[0].data,
            "link": jQuery(this).attr("href")
        };
        data.push(temp);
    });
    var object = {
        "page" : window.location.pathname,
        "data" : data
    };
    localStorage.setItem("level", JSON.stringify(object));
    return object;
}

function loadLevel(){
    var object = JSON.parse(localStorage.getItem("level"));
    if(object == ""){
        return {"page":"","data":[]};
    } else {
        if(window.location.href.indexOf(object["page"]) >= 0){
            return object;
        } else {
            return {"page":"","data":[]};
        }
       
    }
}

function addVariationOne(object){
    var data = object["data"];
    jQuery("#geo_filter").after('<div class="opt-filter-container"></div>');
    jQuery(".opt-filter-container").append('<div class="opt-text"><span>Suggested filters</span></div>');
    jQuery(".opt-filter-container").append('<div class="opt-filter-content"></div>');
    for(var i=0;i< data.length;i++){
        var link = data[i]["link"];
        var active = "";
        if(window.location.pathname == link){
            link = object["page"];
            active = "opt-active"
        }
        if(data[i]["name"].indexOf("Certificate") >= 0){
            var text = data[i]["name"].split(" ")[1];
            text = "Cert " + text;
            data[i]["name"] = text; 
        } 
        if(i < 3){
            jQuery(".opt-filter-content").append('<div class="opt-filter-item '+active+'"><a href="'+link+'">'+data[i]["name"]+'</a></div>');
        }
    }
}

function addVariationTwo(){
    var data = object["data"];
    jQuery("#geo_filter").after('<div class="opt-filter-container"></div>');
    jQuery(".opt-filter-container").append('<div class="opt-text"><span>Suggested filters</span></div>');
    jQuery(".opt-filter-container").append('<div class="opt-filter-content"></div>');
    for(var i=0;i< data.length;i++){
        var link = data[i]["link"];
        var active = "";
        if(window.location.pathname == link){
            link = object["page"];
            active = "opt-active"
        }
        if(data[i]["name"].indexOf("Certificate") >= 0){
            var text = data[i]["name"].split(" ")[1];
            text = "Cert " + text;
            data[i]["name"] = text; 
        } 
        if(i < 3){
            jQuery(".opt-filter-content").append('<div class="opt-filter-item '+active+'"><a href="'+link+'">'+data[i]["name"]+'</a></div>');
        }
    }
}

function addVariationThree(object){
    var data = object["data"];
    jQuery("#geo_filter").after('<div class="opt-filter-container"></div>');
    jQuery(".opt-filter-container").append('<div class="opt-text"><span>Suggested filters</span></div>');
    jQuery(".opt-filter-container").append('<div class="opt-filter-content"></div>');
    for(var i=0;i< data.length;i++){
        var link = data[i]["link"];
        var active = "";
        if(window.location.pathname == link){
            link = object["page"];
            active = "opt-active"
        }
        if(data[i]["name"].indexOf("Certificate") >= 0){
            var text = data[i]["name"].split(" ")[1];
            text = "Cert " + text;
            data[i]["name"] = text; 
        } 
        if(i < 3){
            jQuery(".opt-filter-content").append('<div class="opt-filter-item '+active+'"><a href="'+link+'">'+data[i]["name"]+'</a></div>');
        }
    }
}

function addLevel(object){
    if(object["data"].length != 0) {
        var variation  = 2; 
        jQuery("body").addClass("opt107");
        jQuery("body").addClass("opt107v" + variation);

        //modify filter
        jQuery("#geo_filter.toolbar .filter-btn, .filter-btn").click(function(){
            setTimeout(function(){
                var text = jQuery("#geo_filter.toolbar .filter-btn")[0].childNodes[0].data
                if(text != "Close filters ") {
                    jQuery("#geo_filter.toolbar .filter-btn")[0].childNodes[0].data = "Refine";
                }
            },50);
        });
        jQuery("#geo_filter.toolbar .filter-btn")[0].childNodes[0].data = "Refine";

        switch(variation){
            case 0:
                addVariationOne(object);
                break;
            case 1: 
                addVariationTwo(object);
                break;
            case 2: 
                addVariationThree(object);
                break;
        }

    }
}

defer(function(){  
    if(noFilter()){
        addLevel(saveLevel());
    } else {
        addLevel(loadLevel());
    }
}, "#geo_filter");

function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector) }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector) }, 50);
    }    
}

defer(function(){
    $ = jQuery.noConflict();
    $('body').addClass('OptimizelyTest');

    /**************************************
        OVERVIEW MODULE
    **************************************/
    $(".OptimizelyTest.single-product .overview-box article").wrapInner("<div class='hide' />");
    $(".OptimizelyTest.single-product .overview-box article").prepend("<div class='show' />");
    $(".OptimizelyTest.single-product .overview-box").append("<a href='#' id='toggle-text' class='closed'><span class='fa fa-expand' aria-hidden='true'></span>Expand all and read more</a>");

    var text = $(".OptimizelyTest.single-product .overview-box article .hide p:first-of-type").text();
    $(".OptimizelyTest.single-product .overview-box article .show").prepend("<ul />");
    text = text.split('.');
    for(i = 0; i < text.length; i++){
        if(text[i])
        $(".OptimizelyTest.single-product .overview-box article .show ul").append("<li>" + text[i] + ".</li>");
    };
    var text = $(".OptimizelyTest.single-product .overview-box p:first-of-type").remove();

    $(".OptimizelyTest.single-product .overview-box #toggle-text").click(function(e){
        e.preventDefault();
        $this = $(this);
        if($this.hasClass('closed')){
            $this.removeClass('closed');
            $(".OptimizelyTest.single-product .overview-box #toggle-text").html("<span class='fa fa-compress' aria-hidden='true'></span>Collapse");
            $(".OptimizelyTest.single-product .overview-box article .hide").attr("style", "display: block !important");
        } else {
            $this.addClass('closed');
            $(".OptimizelyTest.single-product .overview-box #toggle-text").html("<span class='fa fa-expand' aria-hidden='true'></span>Expand all and read more");
            $(".OptimizelyTest.single-product .overview-box article .hide").attr("style", "display: none !important");
        };
    });

    $('head').append(`
    <style type="text/css">
        /***** Overview Module *****/
        .OptimizelyTest.single-product .overview-box .hide {display:none !important;}
        .OptimizelyTest.single-product .overview-box #toggle-overview {display:none !important;}
        .OptimizelyTest.single-product .overview-box #toggle-text {color:#00BAC4; margin-top:20px;}
        .OptimizelyTest.single-product .overview-box #toggle-text span {margin-right:3px;}
        .OptimizelyTest.single-product .overview-box ul {padding:0 0 0 18px;}
        .OptimizelyTest.single-product .overview-box ul li {padding:0 0 10px 0;}

        /***** Media Queries *****/
        @media only screen and (max-width: 1260px){
            .OptimizelyTest #popup-container.container {width:96%;}
            .OptimizelyTest .popup-enquiry .enquiry-testimonials .enquiry-testimonials-list .testimonial-item:after {height:calc(100% - 10px); bottom:-4px;}
        }
        
        
        @media only screen and (max-width: 960px){
            .OptimizelyTest .popup-enquiry .enquiry-testimonials {display:none;}
            .OptimizelyTest .popup-enquiry .enquiry-form {float:none; width:auto;}
        }


        @media only screen and (max-width: 769px){
            .OptimizelyTest .course-detail-new .popup-enquiry {padding-left:15px; padding-right:15px;}
            .OptimizelyTest .course-detail-new .popup-enquiry h3 {font-size:22px; line-height:26px; width:calc(100% + 70px); margin-left:-35px; padding:15px 35px 10px 35px; margin-bottom:12px;}
            .OptimizelyTest .course-detail-new .popup-enquiry h3:after {border-width: 30px 15px 0 0; bottom:-30px;}
            .OptimizelyTest .course-detail-new .popup-enquiry h3:before {border-width: 0 15px 30px 0; bottom:-30px;}
            .OptimizelyTest .enquiry-form {border-bottom:5px solid #00bbc5;}    
        }
        
        
        @media only screen and (max-width: 500px){
            .OptimizelyTest .course-detail-new .popup-enquiry h3 {font-size:18px; line-height:24px; padding-top:8px; padding-bottom:5px;}    
        }
    </style>
    `);

}, ".product");
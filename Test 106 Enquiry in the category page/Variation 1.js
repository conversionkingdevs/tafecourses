function loadopt() {
    jQuery('body').addClass('opt106a');
    jQuery(".opt106a #st-results-container > a .row .right-col").append('<button type="button" class="enquire-button optMobile">Enquire</button>');
    jQuery(".opt106a #st-results-container > a .detail-button").before('<button type="button" class="enquire-button optDesktop">Enquire</button>');
	jQuery(".opt106a #st-results-container > a .enquire-button").click(function(e){
      e.preventDefault();
      e.stopPropagation();
      $this = jQuery(this);
      $href = $this.closest('a').attr('href');
      window.location.href = $href+"#open-enquire-form";
    });
  
  	jQuery('#st-results-container a').each(function() {
		jQuery(this).find('.pillbox .pill').each(function() {
			if (jQuery(this).innerWidth() > 160) {
              jQuery(this).parent().addClass('opt106-setheight');
            }
    	});
	});
}
function loadpdp() {
  jQuery('body').addClass('opt106b');
  if(window.location.href.indexOf("open-enquire-form") > -1) {
    loadevents();
  };
}
function loadevents() {
  jQuery( '.entry-title' ).slideUp();
  var enquiryToggle = jQuery( '#course-enquiry-toogle' );
  enquiryToggle.addClass( 'back' );
  enquiryToggle.html( enquiryToggle.hasClass( 'back' ) ? 'Back to Course' : 'Get a free course guide' );
  jQuery( '.course-info' ).slideUp();
  jQuery( '.entry-content' ).slideUp();
  jQuery( 'html,body' ).animate( { scrollTop: 0 }, 'slow' );
  jQuery( '.single-product' ).addClass( 'form-opened' );
  jQuery( '.shade-top' ).hide();
  jQuery( '.enquiry-button' ).hide();
  jQuery( '.product' ).addClass( 'form-opened' );
}
function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}
function loadtest() {

  if (jQuery('body').hasClass('page-template-default')) {
	defer(loadopt, '#st-results-container > a .row .right-col');
  } else {
	defer(loadpdp, '#course-enquiry-toogle');
  }
}

defer(loadtest, 'body');

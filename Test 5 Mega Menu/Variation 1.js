var html = `
<div class="optHeaderCont optFixedPos">
    <div class="optItem" id="findacourse">
        <p>Find a course</p>
    </div>
    <div class="optItem" id="coursebyindustry">
        <p>Course by industry</p>
    </div>
    <div class="optItem" id="courseprovider">
        <p>Course Provider</p>
    </div>
</div>
`,
    menu = `
    
<div class="optHeaderMenu optFixedPos">
    <div class="optDropDownCont" id="findacoursedrop">
        <div>
            <p class="optRedTitle">Online</p>
            <p class="optText">Find a course online that you can study at your own pace</p>
        </div>
        <div>
            <p class="optRedTitle">In-Class</p>
            <p class="optText">Study in-person with your peers and teachers in a collaborative environment</p>
        </div>
    </div>
    <div class="optDropDownCont" id="coursebyindustrydrop">
        <div class="optCol">
            <div>
                <p class="optRedTitle">Arts</p>
                <a class="optText" href="https://www.tafecourses.com.au/courses/arts/">Arts</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/culinary-arts/">Culinary Arts</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/music/">Music</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/language/">Language</a>
            </div>
            <div>
                <p class="optRedTitle">Animal Care</p>
                <a class="optText" href="https://www.tafecourses.com.au/courses/animal-care/">Animal Care</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/veterinary-nursing/">Vertinerary Nursing</a>
            </div>
            <div>
                <p class="optRedTitle">Beauty</p>
                <a class="optText" href="https://www.tafecourses.com.au/courses/beauty/">Beauty</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/beauty-therapy/">Beauty Therapy</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/hairdressing/">Hairdressing</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/massage-therapy/">Massage Therapy</a>
            </div>
        </div>
        <div class="optCol">
            <div>
                <p class="optRedTitle">Business</p>
                <a class="optText" href="https://www.tafecourses.com.au/courses/accounting/">Accounting</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/banking/">Banking</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/bookkeeping/">Bookkeeping</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/business/">Business</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/business-administration/">Business Administration</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/event-management/">Event Management</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/finance/">Finance</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/financial-planning/">Financial Planning</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/management/">Management</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/marketing/">Marketing</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/project-management/">Project Management</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/quality-management/">Quality Management</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/sales/">Sales</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/sap/">SAP</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/security/">Security</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/share-trading/">Share Trading</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/small-business-management/">Small Business Management</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/training-assessment/">Training & Assessment</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/xero/">Xero</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/rg146/">RG146</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/logistics/">Logistics</a>
            </div>
        </div>
         <div class="optCol">
            <div>
                <p class="optRedTitle">Design & Media</p>
                <a class="optText" href="https://www.tafecourses.com.au/courses/design/">Design</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/digital-media/">Digital Media</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/graphic-design/">Graphic Design</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/interior-design/">Interior Design</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/photography/">Photography</a>
            </div>
            <div>
                <p class="optRedTitle">Education</p>
                <a class="optText" href="https://www.tafecourses.com.au/courses/education/">Education</a>
            </div>
            <div>
                <p class="optRedTitle">Environment</p>
                <a class="optText" href="https://www.tafecourses.com.au/courses/agriculture/">Agriculture</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/conservation-and-land-management/">Conservation & Land Management</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/horticulture/">Horticulture</a>
            </div>
        </div>
         <div class="optCol">
            <div>
                <p class="optRedTitle">Health</p>
                <a class="optText" href="https://www.tafecourses.com.au/courses/aged-care/">Aged Care</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/allied-health/">Allied Health</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/child-care/">Child Care</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/disability/">Disability</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/fitness/">Fitness</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/health-sciences/ARTS">Health Sciences</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/healthcare/">Health Care</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/nursing/">Nursing</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/nutrition/">Nutrition</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/work-health-safety/">Work health & Safety</a>
            </div>
            <div>
                <p class="optRedTitle">Hospitality</p>
                <a class="optText" href="https://www.tafecourses.com.au/courses/hospitality/">Hospitality</a>
            </div>
            <div>
                <p class="optRedTitle">Real Estate</p>
                <a class="optText" href="https://www.tafecourses.com.au/courses/real-estate/">Real Estate</a>
            </div>
        </div>
         <div class="optCol">
            <div>
                <p class="optRedTitle">Services</p>
                <a class="optText" href="https://www.tafecourses.com.au/courses/electrical/">Electrical</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/plumbing/">Plumbing</a>
            </div>
            <div>
                <p class="optRedTitle">Social Services</p>
                <a class="optText" href="https://www.tafecourses.com.au/courses/children-services/">Children Services</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/community-services/">Community Services</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/counselling/">Counselling</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/human-resources/">Human Resources</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/psychology/">Psychology</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/youth-work/">Youth Work</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/celebrancy/">Celebrancy</a>
            </div>
            <div>
                <p class="optRedTitle">Sports</p>
                <a class="optText" href="https://www.tafecourses.com.au/courses/sports/">Sports</a>
            </div>
        </div>
         <div class="optCol">
            <div>
                <p class="optRedTitle">Technical</p>
                <a class="optText" href="https://www.tafecourses.com.au/courses/automotive/">Automotive</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/building-construction/">Building & Construction</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/electrical/">Electrical</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/engineering/">Engineering</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/it/">IT</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/programming/">Programming</a>
                <a class="optText" href="https://www.tafecourses.com.au/courses/web-development/">Web Development</a>
            </div>
            <div>
                <p class="optRedTitle">Travel & Tourism</p>
                <a class="optText" href="https://www.tafecourses.com.au/courses/travel-tourism/">Travel & Tourism</a>
            </div>
        </div>
     </div>
     <div class="optDropDownCont" id="courseproviderdrop">
        <div class="optCol">
            <div>
                <a href="https://www.tafecourses.com.au/courses/scope-training/"><img src="https://www.tafecourses.com.au/wp-content/uploads/scope-training-logo.png"/></a>
                <a href="https://www.tafecourses.com.au/courses/tafe-nsw-north-region/"><img src="https://www.tafecourses.com.au/wp-content/uploads/tafe-nsw-250.png"/></a>
                <a href="https://www.tafecourses.com.au/courses/let-training/"><img src="https://www.tafecourses.com.au/wp-content/uploads/let-training-250pxx.png"/></a>
                <a href="https://www.tafecourses.com.au/courses/foundation-education/"><img src="https://www.tafecourses.com.au/wp-content/uploads/foundation-education-brand-logo.png"/></a>
                <a href="https://www.tafecourses.com.au/courses/institute-of-training-and-further-education/"><img src="https://www.tafecourses.com.au/wp-content/uploads/institute-of-training-further-education-250pxx.png"/></a>
                <a href="https://www.tafecourses.com.au/courses/diversitat-training/"><img src="https://www.tafecourses.com.au/wp-content/uploads/diversitat-training-250pxx.png"/></a>
            </div>
        </div>
         <div class="optCol">
            <div>
                <a href="https://www.tafecourses.com.au/courses/open-colleges/"><img src="https://www.tafecourses.com.au/wp-content/uploads/open.png"/></a>
                <a href="https://www.tafecourses.com.au/courses/minerva-college/"><img src="https://www.tafecourses.com.au/wp-content/uploads/MInerva-Logo-1-copy-3.png"/></a>
                <a href="https://www.tafecourses.com.au/courses/interior-design-institute/"><img src="https://www.tafecourses.com.au/wp-content/uploads/logo-3.gif"/></a>
                <a href="https://www.tafecourses.com.au/courses/the-career-academy/"><img src="https://www.tafecourses.com.au/wp-content/uploads/the-career-academy-.png"/></a>
                <a href="https://www.tafecourses.com.au/courses/australis-college/"><img src="https://www.tafecourses.com.au/wp-content/uploads/australis-college-logo-new-250pxx.png"/></a>
                <a href="https://www.tafecourses.com.au/courses/inspire-education/"><img src="https://www.tafecourses.com.au/wp-content/uploads/inspire-education-250pxx.png"/></a>
            </div>
        </div>
         <div class="optCol">
            <div>
                <a href="https://www.tafecourses.com.au/courses/aim-business-school/"><img src="https://www.tafecourses.com.au/wp-content/uploads/AIM-Business-School-logo.png"/></a>
                <a href="https://www.tafecourses.com.au/courses/australian-institute-of-personal-trainers/"><img src="https://www.tafecourses.com.au/wp-content/uploads/AIPT_Logo_whitebackground.png"/></a>
                <a href="https://www.tafecourses.com.au/courses/the-gordon-institute-of-tafe/"><img src="https://www.tafecourses.com.au/wp-content/uploads/the-gordon-250pxx.png"/></a>
                <a href="https://www.tafecourses.com.au/courses/training-for-me/"><img src="https://www.tafecourses.com.au/wp-content/uploads/imageedit_3_7381152529.png"/></a>
                <a href="https://www.tafecourses.com.au/courses/upskilled/"><img src="https://www.tafecourses.com.au/wp-content/uploads/Upskilled-Master-Logo-png.png"/></a>
                <a href="https://www.tafecourses.com.au/courses/train-smart-australia/"><img src="https://www.tafecourses.com.au/wp-content/uploads/train-smart-australia-250pxx.png"/></a>
            </div>
        </div>
    </div>
</div>
`,
    mobilehtml = `
<div class="optMobile"> 
    <ul>
        <li><a>Find a course</a></li>
        <ul class="optSub">
            <li><a href="google">Online</a></li>
            <li><a href="google">In-Class</a></li>
        </ul>
        <li><a>Course By Industry</a></li>
        <ul class="optSub">
                <a class="optRedTitle">Arts</a>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/arts/">Arts</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/culinary-arts/">Culinary Arts</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/music/">Msuic</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/language/">Language</a></li>
                <li><a class="optRedTitle">Animal Care</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/animal-care/">Animal Care</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/veterinary-nursing/">Vertinerary Nursing</a></li>
                <li><a class="optRedTitle">Beauty</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/beauty/">Beauty</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/beauty-therapy/">Beauty Therapy</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/hairdressing/">Hairdressing</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/massage-therapy/">Massage Therapy</a></li>
                <li><a class="optRedTitle">Business</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/accounting/">Accounting</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/banking/">Banking</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/bookkeeping/">Bookkeeping</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/business/">Business</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/business-administration/">Business Administration</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/event-management/">Event Management</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/finance/">Finance</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/financial-planning/">Financial Planning</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/management/">Management</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/marketing/">Marketing</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/project-management/">Project Management</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/quality-management/">Quality Management</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/sales/">Sales</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/sap/">SAP</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/security/">Security</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/share-trading/">Share Trading</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/small-business-management/">Small Business Management</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/training-assessment/">Training & Assessment</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/xero/">Xero</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/rg146/">RG146</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/logistics/">Logistics</a></li>
                <li><a class="optRedTitle">Design & Media</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/design/">Design</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/digital-media/">Digital Media</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/graphic-design/">Graphic Design</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/interior-design/">Interior Design</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/photography/">Photography</a></li>
                <li><a class="optRedTitle">Education</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/education/">Education</a></li>
                <li><a class="optRedTitle">Environment</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/agriculture/">Agriculture</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/conservation-and-land-management/">Conservation & Land Management</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/horticulture/">Horticulture</a></li>
                <li><a class="optRedTitle">Health</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/aged-care/">Aged Care</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/allied-health/">Allied Health</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/child-care/">Child Care</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/disability/">Disability</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/fitness/">Fitness</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/health-sciences/ARTS">Health Sciences</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/healthcare/">Health Care</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/nursing/">Nursing</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/nutrition/">Nutrition</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/work-health-safety/">Work health & Safety</a></li>
                <li><a class="optRedTitle">Hospitality</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/hospitality/">Hospitality</a></li>
                <li><a class="optRedTitle">Real Estate</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/real-estate/">Real Estate</a></li>
                <li><a class="optRedTitle">Services</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/electrical/">Electrical</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/plumbing/">Plumbing</a></li>
                <li><a class="optRedTitle">Social Services</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/children-services/">Children Services</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/community-services/">Community Services</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/counselling/">Counselling</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/human-resources/">Human Resources</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/psychology/">Psychology</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/youth-work/">Youth Work</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/celebrancy/">Celebrancy</a></li>
                <li><a class="optRedTitle">Sports</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/sports/">Sports</a></li>
                <li><a class="optRedTitle">Technical</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/automotive/">Automotive</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/building-construction/">Building & Construction</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/electrical/">Electrical</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/engineering/">Engineering</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/it/">IT</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/programming/">Programming</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/web-development/">Web Development</a></li>
                <li><a class="optRedTitle">Travel & Tourism</a></li>
                <li><a class="optText" href="https://www.tafecourses.com.au/courses/travel-tourism/">Travel & Tourism</a></li>
        </ul>
         <li><a>Course Provider</a></li>
        <ul class="optSub">
                <li><a href="https://www.tafecourses.com.au/courses/scope-training/">Scope Training</a></li>
                <li><a href="https://www.tafecourses.com.au/courses/tafe-nsw-north-region/">Tafe NSW North Region</a></li>
                <li><a href="https://www.tafecourses.com.au/courses/let-training/">Let Training</a></li>
                <li><a href="https://www.tafecourses.com.au/courses/foundation-education/">Foudnation Education</a></li>
                <li><a href="https://www.tafecourses.com.au/courses/institute-of-training-and-further-education/">Institute of Training and Further Education</a></li>
                <li><a href="https://www.tafecourses.com.au/courses/diversitat-training/">Diversitate Training</a></li>
                <li><a href="https://www.tafecourses.com.au/courses/open-colleges/">Open Colleges</a></li>
                <li><a href="https://www.tafecourses.com.au/courses/minerva-college/">Minerva College</a></li>
                <li><a href="https://www.tafecourses.com.au/courses/interior-design-institute/">Interior Design Institute</a></li>
                <li><a href="https://www.tafecourses.com.au/courses/the-career-academy/">The Career Academy</a></li>
                <li><a href="https://www.tafecourses.com.au/courses/australis-college/">Australis College</a></li>
                <li><a href="https://www.tafecourses.com.au/courses/inspire-education/">Inspire Education</a></li>
                <li><a href="https://www.tafecourses.com.au/courses/aim-business-school/">AIM Business School</a></li>
                <li><a href="https://www.tafecourses.com.au/courses/australian-institute-of-personal-trainers/">Australian Institute of Personal Trainers</a></li>
                <li><a href="https://www.tafecourses.com.au/courses/the-gordon-institute-of-tafe/">The Gordon Institute</a></li>
                <li><a href="https://www.tafecourses.com.au/courses/training-for-me/">Training For Me</a></li>
                <li><a href="https://www.tafecourses.com.au/courses/upskilled/">Up Skilled</a></li>
                <li><a href="https://www.tafecourses.com.au/courses/train-smart-australia/">Train Smart</a></li>
        </ul>
    </ul>
</div>
    `


jQuery('body').addClass('opt5'); // Add body class for targetting

jQuery('#cd-top-nav').before(html);
jQuery('#cd-top-nav').before(menu);
jQuery('.cd-navigation').append(mobilehtml);

jQuery('')

jQuery('.optItem').hover(function () {
    var id = jQuery(this).attr('id') + 'drop';
    jQuery(this).addClass('optAfter');
    jQuery('#'+id+', .optHeaderMenu').addClass('optShowing');

}, function () {
   jQuery('.optShowing').removeClass('optShowing');
   jQuery('.optAfter').removeClass('optAfter')
});

jQuery('.optDropDownCont').hover(function () {
    var id = jQuery(this).attr('id').replace('drop','');
    
    jQuery('#'+id).addClass('optAfter');

},function () {
    jQuery('.optAfter').removeClass('optAfter')
});


jQuery('.optMobile > ul > li').click(function (e) {
    e.preventDefault();
    if (jQuery(this).hasClass('optSelected')) {
        jQuery('.optRev').removeClass('optRev');
        jQuery('.optSelected').removeClass('optSelected');
    } else {
        jQuery('.optRev').removeClass('optRev');
        jQuery('.optSelected').removeClass('optSelected');
        jQuery(this).addClass('optSelected');
        jQuery(this).next().addClass('optRev');
    }
});
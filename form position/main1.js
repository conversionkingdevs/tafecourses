

function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer(function(){
    var observer2 = new MutationObserver(function(mutations) {
        mutations.forEach(function(mutation) {
            if(jQuery("#enquiry-form").hasClass("popup-enquiry")){
                jQuery(".opt94-v1 #enquiry-form .enquiry-content").last().prepend(jQuery(".opt94-v1 .right-content .enquiry-form"));
            } else {
                jQuery(".opt94-v1 .right-content").prepend(jQuery(".opt94-v1 #enquiry-form .enquiry-form"));
            }
        });
    });
    var config = { attributes: true};
    if ( window.havePolygon && ( 'geo_warning' === window.geoTargeting || 'geo_restricted' === window.geoTargeting ) ) {
        window._vis_opt_queue = window._vis_opt_queue || [];
        window._vis_opt_queue.push(function() {_vis_opt_goal_conversion(202);});
    } else {
        if(jQuery(window).width() > 750){
            jQuery('body').addClass('opt94-v1');
            observer2.observe(document.getElementById('enquiry-form'), config); 
        }
        jQuery("#enquiry-form .enquiry-form").prependTo(jQuery(".opt94-v1 .right-content"));
    }
}, ".enquiry-form");
/* CUSTOM CODE */
var variation = 2;
var itemName = "opt-96";
var element = ".course-info .left-info, .right-content .job-outcomes .primary-box, .entry-requirements > div:eq(0) > div:eq(0)";
var elements = "#st-results-container > a";
var item = getLocalStorage();
if (item != undefined) {
    item = getLocalStorage()
} else {
    item = []
}
function getLocalStorage() {
    return JSON.parse(localStorage.getItem(itemName))
}
function saveLocalStorage() {
    localStorage.setItem(itemName, JSON.stringify(item))
}
function findItem(id) {
    for (var i = 0; i < item.length; i++) {
        if (item[i][0] == id) {
            return i
        }
    }
    return !1
}

function getItem(id) {
    var index = findItem(id);
    if (index === 0 || index > 0) {
        return item[index][1]
    } else {
        return !1
    }
}

function saveItem(id, data) {
    var index = findItem(id);
    if (index === 0 || index > 0) {
        item[index][1] = data
    } else {
        var temp = [id, data];
        item.push(temp)
    }
    saveLocalStorage()
}

function loadItem(url, callback) {
    jQuery.get(url, function(data) {
        var htmlData = jQuery.parseHTML(data);
        var elementData = jQuery(htmlData).find(element);
        elementData = dataMiddleware(elementData);
        saveItem(url, elementData);
        if (callback) {
            callback(findItem(url))
        } else {
            console.log("add callback to recieve data")
        }
    })
}
function dataMiddleware(data) {
    var tempData = [];
    jQuery(data).each(function(index) {
        var dataObject = {
            v: "",
            data: jQuery(this).html().trim()
        }
        if (jQuery(this).attr("class").indexOf("primary-box") >= 0) {
            dataObject.v = 1;
            dataObject.data = jQuery(this).find("ul").html();
            tempData.push(dataObject);
            console.log("found outcomes")
        } else {
            console.log("no outcomes")
        }
        if (jQuery(this).attr("class").indexOf("new-grey") >= 0) {
            dataObject.v = 2;
            tempData.push(dataObject);
            console.log("found requirements")
        } else {
            console.log("no requirements")
        }
        if (jQuery(this).attr("class").indexOf("left-info") >= 0) {
            dataObject.v = 3;
            tempData.push(dataObject)
        } else {
            console.log("no info")
        }
    });
    return tempData
}
function getItemHandler(url, callback) {
    var temp = getItem(url);
    if (!temp) {
        loadItem(url, callback)
    } else {
        callback(findItem(url))
    }
}
function getItemsAboveWindowBottom(elements, itemOffset) {
    var windowBottom = jQuery(window).height() + jQuery(window).scrollTop();
    var elementsToLoad = [];
    jQuery(elements).not(".opt-loaded").each(function(index) {
        if (jQuery(this).offset().top < windowBottom) {
            elementsToLoad.push(jQuery(this))
        } else if (itemOffset != 0) {
            elementsToLoad.push(jQuery(this));
            itemOffset--
        }
    });
    return elementsToLoad
}
function getVariation(a, v) {
    for (var i = 0; i < item[a][1].length; i++) {
        console.log(item[a][1][i].v);
        if (item[a][1][i].v == v) {
            return item[a][1][i].data
        }
    }
    return ""
}
//do not touch above
jQuery("body").addClass("opt-96");
jQuery("body").addClass("opt-96v"+variation);
function loadingItems(){
    jQuery("#st-results-container a .detail-button").text("View Details");
    var itemOffset = 2;
    var elementsToLoad = getItemsAboveWindowBottom(elements, itemOffset);
    for(var i = 0; i < elementsToLoad.length; i++) {
        var element = jQuery(elementsToLoad[i]);
        var url = jQuery(elementsToLoad[i]).attr("href").split("https://www.tafecourses.com.au").join("");
        jQuery(element).addClass("opt-loading");
        (function(element){
            try{
                getItemHandler(url, function(a){
                    jQuery(element).removeClass("opt-loading");
                    jQuery(element).addClass("opt-loaded");
                    
                    switch(variation){
                        case 0:
                            //Joboutcomes
                            var html = getVariation(a, 1);
                            if(html != ""){
                                jQuery(element).children("button").before('<div class="opt-content"><h3>Job Outcomes</h3></div>');
                                jQuery(element).find(".opt-content").append("<ul>"+html+"</ul>");
                            }
                            break;
                        case 1:
                            //Entry Requirements
                            var html = getVariation(a, 2);
                            if(html != ""){
                                jQuery(element).children("button").before('<div class="opt-content opt-requirements"><h3>Entry Requirements</h3></div>');
                                jQuery(element).find(".opt-content").append("<div>"+html+"</div>");
                            }
                            break;
                        case 2:
                            //Main Details
                            var html = getVariation(a, 3);
                            if(html != ""){
                                jQuery(element).children("button").before('<div class="opt-content"></div>');
                                jQuery(element).find(".opt-content").append("<div>"+html+"</div>");
                            }
                            break;
                        default:
                            break;
                    }
                });
            } catch(e){
                //console.log("Exception: " + e + "occured");
            }
        } (element))
    }
}
loadingItems();
setInterval(function(){
    if(jQuery(".opt-loading").length == 0){
        loadingItems();
    }
},1000);
jQuery(window).scroll(function(){
    if(jQuery(".opt-loading").length == 0){
        loadingItems();
    }
});

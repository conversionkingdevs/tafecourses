function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
}
if(window.location.pathname == "/enquiry/"){
    $(".product-template-default");
    console.clear();
    $ = jQuery.noConflict();
    $('body').addClass('OptimizelyTest');

    $("#gform_1").attr("action",getURLParameter('formaction'));

    /***** Form Focus Arrow *****/
    $(".OptimizelyTest .gform_wrapper input, .OptimizelyTest .gform_wrapper select, .OptimizelyTest .gform_wrapper textarea").focus(function(){
        $offsetTop = $(this).parents('.gfield').offset().top - 79;
        $(".OptimizelyTest .col-md-6.enquiry-content .form-arrow").css('top',$offsetTop);
        $(".OptimizelyTest .enquiry-content-left").css('min-height',$(".OptimizelyTest .enquiry-content").height());
    });
    
    $(".OptimizelyTest .gform_wrapper input, .OptimizelyTest .gform_wrapper select, .OptimizelyTest .gform_wrapper textarea").blur(function(){
        $(".OptimizelyTest .enquiry-content-left").css('min-height',$(".OptimizelyTest .enquiry-content").height());
    });

    var desktopImages = {
        "oneimage": "//useruploads.visualwebsiteoptimizer.com/useruploads/302000/images/a96dc635bbd6ad1f6e3f277d9c593db2_marlon.png",
        "twoimage": "//useruploads.visualwebsiteoptimizer.com/useruploads/302000/images/3e20712929a75207c09c8afde4031eea_dimity.png",
        "threeimage": "//useruploads.visualwebsiteoptimizer.com/useruploads/302000/images/cfe8bdcb9566a632d532326b21cc21d4_soula.png",
        "fourimage": "//useruploads.visualwebsiteoptimizer.com/useruploads/302000/images/255e09ec6d17d8e5ff05c4c35aff88ac_kylie.png"
    };

    jQuery("li[data-id='quote1'] img").attr("src", desktopImages['oneimage']);
    jQuery("li[data-id='quote2'] img").attr("src", desktopImages['twoimage']);
    jQuery("li[data-id='quote3'] img").attr("src", desktopImages['threeimage']);
    jQuery("li[data-id='quote4'] img").attr("src", desktopImages['fourimage']);
} else {
    function getPostID(){
        var _class = $("body").attr("class").split(" ");
        var postid = "";
        for(var i = 0; i < _class.length; i++){
            if(_class[i].indexOf("postid") >= 0 ){
                postid = _class[i].split("-")[1];
            }
        }
        return postid;
    }
    if($(".product-template-default").length > 0){
        $(".course-guide-button, .course-enquiry-toogle").click(function(e){
            e.stopPropagation();
            var title = jQuery("h1.entry-title").text().split(" ");
            title.pop();
            title = title.join("%20");
            var formAction  = $("#gform_1").attr("action");
            window.location.href = "https://www.tafecourses.com.au/enquiry/?postid=" + getPostID() + "&cname=" + title + "&formaction=" + formAction;
        });
        $("#enquiry-form, .enquiry-form").hide();
    }
}
       
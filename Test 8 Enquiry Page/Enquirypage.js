function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
}
if(window.location.pathname == "/enquiry/"){
    $(".product-template-default");
    console.clear();
    $ = jQuery.noConflict();
    $('body').addClass('OptimizelyTest');
    
    var cname = getURLParameter('cname');
    
    
    /**************************************
        LAYOUT
    **************************************/ 
    
    /***** Header *****/
    $(".OptimizelyTest .site-container").prepend('<header class="site-header" itemscope="" itemtype="https://schema.org/WPHeader"><div itemscope="" itemtype="http://schema.org/Service"><a id="cd-logo" href="/" title="TAFE Courses Australia" itemprop="logo">&nbsp;</a></div></header>');
    
    
    /***** Footer *****/
    $(".OptimizelyTest .site-container .site-inner").append('<footer class="site-footer"itemscope=""itemtype="https://schema.org/WPFooter"><div class="footer-two"><div class="container"><div class="footer-listing"><div><div><h3>Courses</h3><ul class="subject-listing-footer"><li class="list-split"><ul><li><a href="/courses/accounting/"target="_blank"title="Accounting Courses">Accounting</a></li><li><a href="/courses/adelaide/"target="_blank"title="Adelaide Courses">Adelaide</a></li><li><a href="/courses/aged-care/"target="_blank"title="Aged Care Courses">Aged Care</a></li><li><a href="/courses/beauty-therapy/"target="_blank"title="Beauty Therapy Courses">Beauty Therapy</a></li><li><a href="/courses/brisbane/"target="_blank"title="Brisbane Courses">Brisbane</a></li><li><a href="/courses/business/"target="_blank"title="Business Courses">Business</a></li><li><a href="/courses/certificate-iv/"target="_blank"title="Certificate IV Courses">Certificate</a></li><li><a href="/courses/counselling/"target="_blank"title="Counselling Courses">Counselling</a></li></ul></li><li class="list-split"><ul><li><a href="/courses/design/"target="_blank"title="Design Courses">Design</a></li><li><a href="/courses/diploma/"target="_blank"title="Diploma Courses">Diploma</a></li><li><a href="/courses/education/"target="_blank"title="Education Courses">Education</a></li><li><a href="/courses/engineering/"target="_blank"title="Engineering Courses">Engineering</a></li><li><a href="/courses/finance/"target="_blank"title="Finance Courses">Finance</a></li><li><a href="/courses/financial-planning/"target="_blank"title="Financial Planning Courses">Financial Planning</a></li><li><a href="/courses/fitness/"target="_blank"title="Fitness Courses">Fitness</a></li></ul></li><li class="list-split"><ul><li><a href="/courses/healthcare/"target="_blank"title="Healthcare Courses">Healthcare</a></li><li><a href="/courses/hospitality/"target="_blank"title="Hospitality Courses">Hospitality</a></li><li><a href="/courses/it/"target="_blank"title="IT Courses">IT</a></li><li><a href="/courses/management/"target="_blank"title="Management Courses">Management</a></li><li><a href="/courses/marketing/"target="_blank"title="Marketing Courses">Marketing</a></li><li><a href="/courses/melbourne/"target="_blank"title="Melbourne Courses">Melbourne</a></li><li><a href="/courses/online/"target="_blank"title="Online Courses">Online</a></li></ul></li><li class="list-split"><ul><li><a href="/courses/perth/"target="_blank"title="Perth Courses">Perth</a></li><li><a href="/courses/photography/"target="_blank"title="Photography Courses">Photography</a></li><li><a href="/courses/postgraduate-certificate/"target="_blank"title="Postgraduate Courses">Postgraduate</a></li><li><a href="/courses/project-management/"target="_blank"title="Project Management">Project Management</a></li><li><a href="/courses/short/"target="_blank"title="Short Courses">Short Courses</a></li><li><a href="/courses/sydney/"target="_blank"title="Sydney Courses">Sydney</a></li><li><a href="/courses/tourism/"target="_blank"title="Tourism Courses">Tourism</a></li></ul></li></ul></div><div><h3>Course Providers</h3><ul class="provider-listing-footer"><li class="list-split"><ul><li><a href="/courses/oten/"target="_blank"title="OTEN - Open Technology &amp;Education Network">OTEN TAFE</a></li><li><a href="/courses/open-colleges/"target="_blank"title="Open Colleges">Open Colleges</a></li></ul></li><li class="list-split"><ul><li><a href="/courses/the-gordon-institute-of-tafe/"target="_blank"title="The Gordon TAFE">The Gordon TAFE</a></li></ul></li><li class="list-split"><ul><li><a href="/courses/kangan-institute/"target="_blank"title="Kangan Institute of TAFE">Kangan Institute of TAFE</a></li></ul></li><li class="list-split"><ul><li><a href="/courses/bendigo-tafe/"target="_blank"title="Bendigo TAFE">Bendigo TAFE</a></li></ul></li></ul><h3>Popular Now</h3><ul class="popularnow-listing-footer"><li class="list-split"><ul><li><a href="/resources/guide-to-tafe-courses-in-western-australia/"target="_blank"title="TAFE WA">TAFE WA</a></li><li><a href="/resources/guide-to-tafe-courses-in-canberra/"target="_blank"title="CIT Canberra">CIT Canberra</a></li></ul></li><li class="list-split"><ul><li><a href="/resources/tafe-in-tasmania-tas/"target="_blank"title="TAFE Tasmania">TAFE Tasmania</a></li><li><a href="/resources/guide-to-tafe-courses-in-sunshine-coast/"target="_blank"title="East Coast TAFE">East Coast TAFE</a></li></ul></li><li class="list-split"><ul><li><a href="/resources/guide-to-tafe-courses-in-cairns/"target="_blank"title="TAFE North">TAFE North</a></li></ul></li><li class="list-split"><ul><li><a href="/resources/the-ultimate-guide-to-wodonga-tafe/"target="_blank"title="Wodonga TAFE">Wodonga TAFE</a></li></ul></li></ul><h3>Popular Courses</h3><ul class="popular-listing-footer"><li class="list-split"><ul><li><a href="/course/diploma-of-counselling/"title="Diploma of Counselling">Diploma of Counselling</a></li><li><a href="/course/certificate-iii-in-education-support/"title="Certificate III in Education Support (Teacher`s Aide)">Certificate III in Education Support (Teacher`s Aide)</a></li><li><a href="/course/certificate-iv-in-veterinary-nursing/"title="Certificate IV in Veterinary Nursing">Certificate IV in Veterinary Nursing</a></li></ul></li><li class="list-split"><ul><li><a href="/course/certificate-iv-in-training-and-assessment/"title="Certificate IV in Training and Assessment">Certificate IV in Training and Assessment</a></li><li><a href="/course/certificate-iv-in-beauty-therapy/"title="Certificate IV in Beauty Therapy">Certificate IV in Beauty Therapy</a></li><li><a href="/course/certificate-iii-in-aged-care/"title="Certificate III in Aged Care">Certificate III in Aged Care</a></li></ul></li><li class="list-split"><ul><li><a href="/course/diploma-of-management/"title="Diploma of Management">Diploma of Management</a></li><li><a href="/course/diploma-of-financial-planning/"title="Diploma of Financial Planning">Diploma of Financial Planning</a></li><li><a href="/course/certificate-iv-in-veterinary-nursing/"title="Diploma of Veterinary Nursing">Diploma of Veterinary Nursing</a></li></ul></li></ul></div></div></div></div></div><div class="footer-three"><div class="container"><nav class="container"><a>Copyright © 2016 TafeCourses.com.au</a><a href="/about/">&nbsp;About&nbsp;</a><a href="/contact/">Contact</a><a href="/privacy/">Privacy</a><a href="/terms/">Terms</a><a href="/resources/">Resources</a><a href="/courses/">Courses</a></nav></div></div></footer>');
    
    
    /***** Content *****/
    $(".OptimizelyTest #popup-container .col-md-6.enquiry-content").prepend('<div class="form-arrow" ></div>');
    $(".OptimizelyTest #popup-container").prepend('<div class="col-md-6 enquiry-content-left" ></div>');
    $(".OptimizelyTest #popup-container").append('<div class="clear" ></div>');
    $(".OptimizelyTest #popup-container .enquiry-form h3.purple-color").text('Fill the form to get your course guide!');
    
    
    $(".OptimizelyTest .enquiry-content-left").append('<div class="inner-content" ></div>');
    $(".OptimizelyTest .enquiry-content-left").append('<div class="clear" ></div>');
    $(".OptimizelyTest .enquiry-content-left .inner-content").append('<div class="top-content" ></div>');
    $(".OptimizelyTest .enquiry-content-left .inner-content").append('<div class="testimonials-wrapper" ></div>');
    $(".OptimizelyTest .enquiry-content").append('<div class="testimonials-wrapper" ></div>');
    $(".OptimizelyTest .enquiry-content-left .inner-content .top-content").append('<div class="title" ></div>');
    $(".OptimizelyTest .enquiry-content-left .inner-content .top-content").append('<div class="columns" ></div>');
    
    
    
    $(".OptimizelyTest .enquiry-content-left .title").append('<h1>Get the course guide</h1>');
    $(".OptimizelyTest .enquiry-content-left .title").append('<h2>'+cname+'</h2>');
    $(".OptimizelyTest .enquiry-content-left .title").append('<hr />');
    $("#gform_1").attr("action",getURLParameter('formaction'));
    
    $(".OptimizelyTest .enquiry-content-left .columns").append('<div class="column column-left" ></div>');
    $(".OptimizelyTest .enquiry-content-left .columns").append('<div class="column column-right" ></div>');
    $(".OptimizelyTest .enquiry-content-left .columns").append('<div class="clear" ></div><hr />');
    $(".OptimizelyTest .enquiry-content-left .columns .column-left").append('<img src="https://i.imgur.com/rTyHtdT.png" />');
    $(".OptimizelyTest .enquiry-content-left .columns .column-right").append('<h3>Get more Information</h3>');
    $(".OptimizelyTest .enquiry-content-left .columns .column-right").append('<ul><li>Price & Payment</li><li>Start Dates</li><li>Subjects</li><li>Job Outcomes</li></ul>');
    $(".OptimizelyTest .enquiry-content-left .columns .column-right").append('<h3>Receive a free consultation</h3>');
    $(".OptimizelyTest .enquiry-content-left .columns .column-right").append('<p>After downloading the course guide, receive a free consultation (optional) with one of our course experts</p>');
    $(".OptimizelyTest .enquiry-content-left .columns .column-right").append('<ul><li>Discuss career pathways</li><li>Explore work experience</li><li>Advice on employment topics</li><li>Pre-enrolling information</li></ul>');
    
    $(".OptimizelyTest .testimonials-wrapper").append('<div class="testimonials-navigation" ></div>');
    $(".OptimizelyTest .testimonials-wrapper").append('<div class="testimonials-tabs" ></div>');
    var placeholder = "Hello, I would like to enquire about "+cname+". Can you please provide me with some more information?";
    $(".OptimizelyTest #input_1_7").val(placeholder);
    
    /***** Form Focus Arrow *****/
    $(".OptimizelyTest .gform_wrapper input, .OptimizelyTest .gform_wrapper select, .OptimizelyTest .gform_wrapper textarea").focus(function(){
        $offsetTop = $(this).parents('.gfield').offset().top - 79;
        $(".OptimizelyTest .col-md-6.enquiry-content .form-arrow").css('top',$offsetTop);
        $(".OptimizelyTest .enquiry-content-left").css('min-height',$(".OptimizelyTest .enquiry-content").height());
    });
    
    $(".OptimizelyTest .gform_wrapper input, .OptimizelyTest .gform_wrapper select, .OptimizelyTest .gform_wrapper textarea").blur(function(){
        $(".OptimizelyTest .enquiry-content-left").css('min-height',$(".OptimizelyTest .enquiry-content").height());
    });
    $(".OptimizelyTest #input_1_7").focus(function(){   
        if($(this).val() == placeholder){
            $(this).val("");
        }
    });
    $(".OptimizelyTest #input_1_7").blur(function(){
        if($(this).val() == ""){
            $(this).val(placeholder);
        }
    });
    function removeDisabled(){
        if(jQuery(".gfield_error").length == 0){
            $("#gform_submit_button_1").removeAttr("disabled");
        }
    }
    $(".OptimizelyTest #gform_fields_1 input").change(function(){
        removeDisabled();
    });
    $(".OptimizelyTest #gform_fields_1 select").change(function(){
        removeDisabled();
    });
    $("#gform_1").submit(function(){
        removeDisabled();
    });
    
    
    
    $(window).resize(function(){
        $(".OptimizelyTest .enquiry-content-left").css('min-height',$(".OptimizelyTest .enquiry-content").height());
    });
    /**************************************
        Testimonials DATA
    **************************************/
  var desktopImages = {
    "oneimage": "//useruploads.visualwebsiteoptimizer.com/useruploads/302000/images/a96dc635bbd6ad1f6e3f277d9c593db2_marlon.png",
    "twoimage": "//useruploads.visualwebsiteoptimizer.com/useruploads/302000/images/3e20712929a75207c09c8afde4031eea_dimity.png",
    "threeimage": "//useruploads.visualwebsiteoptimizer.com/useruploads/302000/images/cfe8bdcb9566a632d532326b21cc21d4_soula.png",
    "fourimage": "//useruploads.visualwebsiteoptimizer.com/useruploads/302000/images/255e09ec6d17d8e5ff05c4c35aff88ac_kylie.png"
};
   var  testimonials_data = {
        'testimonials' : {
            '1' : {
                'image': desktopImages['oneimage'],
                'cite': "DIMITY G",
                'blockquote'  : "I found the website easy to use and the contact I received after my enquiry was good.  Not too pushy but there if I needed them"
            },
            '2' : {
                'image': desktopImages['twoimage'],
                'cite': "Marlon M",
                'blockquote'  : "All details and info was there and my query was answered quickly."
            },
            '3' : {
                'image': desktopImages['threeimage'],
                'cite': "Soula T",
                'blockquote'  : "Gave me everything I needed and someone called me, pretty much straight away."
            },
            '4' : {
                'image': desktopImages['fourimage'],
                'cite': "Kylie B",
                'blockquote'  : "Using this website to find a course I am considering as my career was made very easy to do, they made it a easy experience to make sure I was making the right steps into my future. I am pleased to have come across this website and would highly recommended it to anyone who is looking to find the right course."
            }
        }
    };
    
    TestimonialsData(testimonials_data);
    
    
    
    /**************************************
        Testimonials MODULE
    **************************************/
    function TestimonialsData(data){
        var i=1; while(data['testimonials'][i]){
            $(".OptimizelyTest .testimonials-navigation").append('<div class="testimonial-avatar-wrap testimonial-avatar-wrap-'+i+'"><a href="#" class="testimonial-avatar testimonial-avatar-'+i+'"></a></div>');
            $(".OptimizelyTest .testimonials-navigation .testimonial-avatar-"+i).append('<img src="'+data['testimonials'][i]['image']+'" />');
            
            $(".OptimizelyTest .testimonials-tabs").append('<div class="testimonial testimonial-'+i+'">');
            $(".OptimizelyTest .testimonials-tabs .testimonial-"+i).append('<cite>- '+data['testimonials'][i]['cite']+'</cite>');
            $(".OptimizelyTest .testimonials-tabs .testimonial-"+i).append('<blockquote>'+data['testimonials'][i]['blockquote']+'</blockquote>');
            i++;
        };  
    };
    
    
    $(".OptimizelyTest .testimonial-avatar-wrap-1").addClass('active');
    $(".OptimizelyTest .testimonial-1").addClass('active');
    
    
    $(".OptimizelyTest .testimonials-navigation a").click(function(e){
        e.preventDefault();
        $this = $(this);
        $parent = $this.parent();
        $eq = $parent.index()+1;
        $(".OptimizelyTest .testimonials-navigation .testimonial-avatar-wrap").removeClass('active');
        $parent.addClass('active');
        $(".OptimizelyTest .testimonials-tabs .testimonial").removeClass('active');
        $(".OptimizelyTest .testimonials-tabs .testimonial-"+$eq).addClass('active');
    });
    
    
    
    /**************************************
        STYLES
    **************************************/
} else {
    function getPostID(){
        var _class = $("body").attr("class").split(" ");
        var postid = "";
        for(var i = 0; i < _class.length; i++){
            if(_class[i].indexOf("postid") >= 0 ){
                postid = _class[i].split("-")[1];
            }
        }
        return postid
    }
    if($(".product-template-default").length > 0){
        $(".course-guide-button, .course-enquiry-toogle").click(function(e){
            e.stopPropagation();
            var title = jQuery("h1.entry-title").text().split(" ");
            title.pop();
            title = title.join("%20");
            var formAction  = $("#gform_1").attr("action");
            window.location.href = "https://www.tafecourses.com.au/enquiry/?postid=" + getPostID() + "&cname=" + title + "&formaction=" + formAction;
        });
        $("#enquiry-form, .enquiry-form").hide();
    }
}
